from typing import NoReturn
from urllib.parse import urlparse, parse_qs
import requests
from bs4 import BeautifulSoup

from moreawful import moreexcept
from moreawful import moreparser


class SomethingAwfulAPI():
      """
      A definitive python library for interacting with Something Awful.

      Optionally takes existing credentials obtained via dump_creds()

      TODO:
      + Listing Forums
      + Listing Forum Pages
      - Threads and pagination
      - User account viewers
      - Search
      - Voting
      - Replying (Quotes, )
      - Posting (Quotes, bbcode, emoji index)
      """
      def __init__(self, creds=None):
            self.baseurl = 'https://forums.somethingawful.com'
            self.creds = creds if creds else {'cookies': None, 'ma': None}
            self.parser = moreparser.SomethingAwfulParser()

      def __parse_query_string(self, url: str) -> dict:
            """
            Parse a query string into a dictionary
            """
            parse_result = urlparse(url)
            query = parse_qs(parse_result.query)
            # We have to unpack the shitty lists that it puts everything in
            result = { k: v[0] for k, v in query.items() }
            return result

      def __parse_cookies(self, r):
            """
            Parse and join the request and response cookies into a format that
            we can send. The redirect that SA does is sucky because they're in
            two seperately-formatted lists, we can't treat them the same.
            """
            # Turn a 'Cookie' header string into a dict by splitting on the
            # boundaries, then splitting on the first occurence of '='
            cookie = r.request.headers['Cookie'].split('; ')
            cookies = list(map(lambda s: s.split('=', 1), cookie))
            request_cookies = {name: value for name, value in cookies}

            response_cookies = dict(r.cookies)
            # Join the cookies we got from the request and response
            return {**request_cookies, **response_cookies}

      def __raise_login_error(self, request):
            """
            Raise the appropriate error (if any) upon logging in.
            """
            if request.ok: return

            qs = self.__parse_query_string(request.url)
            if 'loginerror' in qs:
                  error_code = int(qs['loginerror'])
                  raise SALoginError(error_code)
            else:
                  request.raise_for_status()

      def dump_creds(self):
            """
            Retrieve the credentials stored internally
            """
            return self.creds

      def login(self, username: str, password: str) -> NoReturn:
            """
            Authenticate with the Forums and automagically store the credentials
            in the SomethingAwfulAPI object.
            """
            url = f'{self.baseurl}/account.php'
            data = {
                  'action': 'login',
                  'username': username,
                  'password': password
            }
            r = requests.post(url, data=data)
            self.__raise_login_error(r)
            # Requests follows the 302 redirect that SA gives us, but that means
            # that the password and username hash cookies are not given to us,
            # we have to grab it manually
            if not r.request.headers['Cookie']:
                  raise SAInternalLoginError("No cookie request header??")
            self.creds['cookies'] = self.__parse_cookies(r)
            self.creds['ma'] = self.parser.logout_csrf(r.content)

      def logout(self):
            """
            De-Authenticate with the forums using the stored ma we got during
            login.
            """
            url = f'{self.baseurl}/account.php'
            data = {
                  'action': 'logout',
                  'ma': self.creds['ma']
            }
            r = requests.post(url, data=data, cookies=self.creds['cookies'])
            r.raise_for_status()
            self.creds = { 'cookies': None, 'ma': None }

      def get_index(self):
            """
            Get an index of the forums
            """
            url = self.baseurl
            cookies = self.creds['cookies']
            r = requests.get(url, cookies=cookies)
            r.raise_for_status()
            return self.parser.forums_index(r.content)

      def get_forum(self, forum_id, icon_id=None):
            """
            Get an index of the forums. Optionally filter by icon_id
            """
            url = f'{self.baseurl}/forumdisplay.php'
            query = {
                  'forumid': forum_id,
                  'posticon': icon_id
            }
            cookies = self.creds['cookies']
            r = requests.get(url, cookies=cookies, params=query)
            r.raise_for_status()
            return self.parser.forums_page(r.content)

      def get_thread_page(self, thread_id, page=1, last_post=False):
            """
            Get a page from a thread. Optionally get the last post ID
            """
