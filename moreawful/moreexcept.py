class SAError(Exception):
      def __str__(self):
            return self.message

class SALoginError(SAError):
      def __init__(self, code):
            self.messages = [
                  '''Something happened and we couldn't log you in!
                     That's a bummer :(''',
                  '''Failed to enter phrase from the security image.''',
                  '''The password you entered is wrong. Remember passwords are
                     case-sensitive! Be careful... too many wrong passwords and
                     you will be locked out temporarily.''',
                  '''The username you entered is wrong, maybe you should try
                     'luser' instead? Watch out... too many failed login
                     attempts and you will be locked out temporarily.''',
                  '''You've made too many failed login attempts. Your IP address
                     is temporarily blocked.''',
            ]
            # If code isn't between 1 - 4, drop back to the default code
            code = code if code > 0 and code < len(self.messages) else 0
            # Clean newlines
            self.message = ''.join(self.messages[code].splitlines())
            # Clean leading spaces
            self.message = ''.join(list(filter(None, self.message.split('  '))))

class SAInternalLoginError(SAError):
      def __init__(self, message):
            self.message = message
