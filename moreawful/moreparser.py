from bs4 import BeautifulSoup
from pymaybe import maybe
from itertools import repeat
from dateutil import parser as dateutil_p

# None of this code is 'safe', because if there is an error it's actually better
# to crash and burn. ITT we take the methodology of Erlang and prefer to fail
# early. The alternative makes the code _hideous_ and it is already fucking
# awful. There's no standardization in how fields are represented so we have
# to redo a fuckload of parsing to get the same data returned from a different
# .php page. Disgusting.
#
# If it crashes and burns please for the love of god submit a bug report. That
# means that the php is not returning what we expect, which should be fixed.

class SomethingAwfulParser():
      """
      An internal library for parsing the SA forums HTML code.

      """
      def __init__(self):
            pass

      def logout_csrf(self, webpage):
            """
            Grab the CRSF for logging out actions
            """
            # logout: <a href="...">Log Out</a> ->
            # href: '/account.php?action=logout&amp;ma=0a1af000'
            # ma: '0a1af000'
            soup = BeautifulSoup(webpage, features='lxml')
            logout = soup.find('ul', id='navigation').find('a', text='Log Out')
            href = logout['href']
            # This should be robust against large-scale changes to this link
            # Split into key/values ('ma=whatever'), split that into a dict
            ma = dict(map(lambda s: s.split('=',1), href.split('&')))['ma']
            return ma

      def __forum_from_index(self, forum):
            """
            Parse an individual forum item from the index
            """
            # ** Grab name and forum_id from main link
            name_tag = forum.find('a', class_='forum')
            name = name_tag.string
            forum_id = name_tag['href'].split('=')[1]

            # ** Grab description and slice it to remove a leading ' - '
            description = forum.find('span', class_='forumdesc').string[3:]

            # ** Grab image URL and usage totals from img tag
            tag = forum.find('img')
            totals = tag.get('title')
            url = 'https:' + tag.get('src')

            # ** Grab subforums list from div
            subforums = []
            subforums_tag = maybe(forum.find('div', class_='subforums'))
            subforums_tags = subforums_tag('a').or_else([])
            for item in subforums_tags:
                  subforum_id = item['href'].split('forumid=')[1]
                  sub_name = item.string
                  subforums.append({'forum_id': subforum_id, 'name': sub_name})

            # ** Grab moderators list from div
            moderators = []
            mod_tags = forum.find('td', class_='moderators').find_all('a')
            mod_tags = maybe(mod_tags).or_else([])
            for item in mod_tags:
                  user_id = item['href'].split('userid=')[1]
                  mod_name = item.string
                  moderators.append({ 'user_id': user_id, 'name': mod_name })

            return {
                  'name': name,
                  'forum_id': forum_id,
                  'description': description,
                  'image': url,
                  'totals': totals,
                  'subforums': subforums,
                  'moderators': moderators
            }

      def __forum_thread_item(self, thread):
            """
            Parse a single thread listing from the index
            """
            # ** visited info is stored in the balls, uh, thread title
            visited = 'seen' in thread['class']

            # ** starred info is stored in the class field ??
            starred = 'bm0' in thread.find(class_='star')['class']

            # ** icon info.... uhghhhhh
            icon_tag = thread.find(class_='icon')
            icon_id = icon_tag.find('a')['href'].split('posticon=')[1]
            icon_url = icon_tag.find('img')['src']
            icon_name = icon_tag.find('img')['alt']

            # ** stickied
            title_tag = thread.find(class_='title')
            stickied = 'title_sticky' in title_tag

            # ** thread title
            thread_title = title_tag.find('a', class_='thread_title')
            name = thread_title.string
            thread_id = thread_title['href'].split('threadid=')[1]

            # ** maximum number of pages in thread
            pages = title_tag(class_='pagenumber')
            # Get the second-but last element. The last element is Last Post,
            # which is not useful to us.
            max_pages = int(maybe(pages)[-2].string.or_else("1"))

            # ** author information
            author_tag = thread.find(class_='author').find('a')
            author_id = author_tag['href'].split('userid=')[1]
            author_name = author_tag.string

            # ** replies and views
            replies = int(thread.find(class_='replies').find('a').string)
            views = int(thread.find(class_='views').string)

            # ** rating info
            rating_tag = thread.find(class_='rating').find('img')
            if not rating_tag:
                  rating = {}
            else:
                  rating = {
                        'info': rating_tag['title'],
                        'url': rating_tag['src']
                  }

            # last post info
            lastpost_tag = thread.find(class_='lastpost')
            lastdate = dateutil_p.parse(lastpost_tag.find(class_='date').string)
            lastpost_author_tag = lastpost_tag.find(class_='author')
            lastauthor_id = lastpost_author_tag['href'].split('threadid=')[1]
            lastauthor_name = lastpost_author_tag.string

            return {
                  'thread_id': thread_id,
                  'visited': visited,
                  'starred': starred,
                  'icon': {
                        'icon_id': icon_id,
                        'url': icon_url,
                        'name': icon_name
                  },
                  'stickied': stickied,
                  'name': name,
                  'max_pages': max_pages,
                  'author': {
                        'user_id': author_id,
                        'name': author_name
                  },
                  'replies': replies,
                  'views': views,
                  'rating': rating,
                  'killed_by': {
                        'name': lastauthor_name,
                        'user_id': lastauthor_id,
                        'datetime': lastdate
                  }

            }

      def __forum_page_info(self, webpage):
            """
            Parse the forum index
            """
            # Unfortunately due to different information being present we can't
            # actually wrap these into smaller pieces without making the entire
            # thing ridiculous. Jesus christ this is *disgusting*

            # ** Get name, number of users browsing
            tag = webpage.find('a', class_='bclast')
            name = tag.string

            tag = webpage.find('span', class_='online_users').find('a')
            browsing = int(tag.string.split(' ')[0])

            # ** Get current page and max number of pages
            tag = webpage.find('div', class_='pages').find('select')
            selected = tag.find(selected='selected')
            current_page = int(selected.string)
            max_pages = int(list(tag)[-1].string)

            # ** Get list of subforums
            tag = maybe(webpage.find('table', id='subforums')).find('tbody')
            # We have the titles, topics, and posts independently
            topics  = [int(t.string) for t in tag(class_='topics')]
            posts   = [int(t.string) for t in tag(class_='posts')]
            descs   = [t.string[3:]  for t in tag('dd')]
            titles  = [t.find('a').string for t in tag(class_='title')]
            links   = [t['href'].split('forumid=')[1] for t in tag('a')]

            # Zip them together
            subforums = list(zip(
                  zip(repeat('forum_id'),    links),
                  zip(repeat('description'), descs),
                  zip(repeat('title'),       titles),
                  zip(repeat('topics'),      topics),
                  zip(repeat('posts'),       posts)
            ))
            subforums = [dict(x) for x in subforums]

            # ** Get list of moderators
            tag = webpage.find(id='mods')('a')
            names = [t.string for t in tag]
            user_ids = [t['href'].split('userid=')[1] for t in tag]
            moderators = list(zip(
                  zip(repeat('name'), names),
                  zip(repeat('user_id'), user_ids)
            ))
            moderators = [dict(x) for x in moderators]

            # ** Get list of threads
            threads = []
            for thread in webpage(class_='thread'):
                  threads.append(self.__forum_thread_item(thread))

            return {
                  'name': name,
                  'browsing': browsing,
                  'current_page': current_page,
                  'max_pages': max_pages,
                  'subforums': subforums,
                  'moderators': moderators,
                  'threads': threads
            }

      def forums_index(self, webpage):
            """
            Grab the forums index.
            Returns an list of dicts like:

            [
                  {'name': 'General Bullshit',
                   'forum_id': '273',
                   'description': 'Look at this pigs huge balls',
                   'image': 'https://.../forumicons/gbs.gif',
                   'totals': '5089103 replies in 21728 threads',
                   'subforums': [{'forum_id': '669',
                                  'name': 'The Cholesterol Clubhouse'}]
                   'moderators': [{'user_id': '...', 'name': '...'}]}
            ]
            """
            soup = BeautifulSoup(webpage, features='lxml')
            forums = soup('tr', class_='forum')
            index = []
            for forum in forums:
                  index.append(self.__forum_from_index(forum))
            return index

      def forums_page(self, webpage):
            """
            Grab the page of a forum or subforum
            Returns a list of dicts like:

            [
                  {'name': 'General Bullshit',
                   'browsing': 88,
                   'current_page': 1,
                   'max_pages': 15,
                   'subforums': [{'forum_id': '669',
                                  'name': 'The Cholesterol Clubhouse',
                                  'description': 'Bring your own Bun',
                                  'topics': 200,
                                  'posts': 1080}],
                   'moderators': [{'user_id': '...', 'name': '...'}],
                   'threads': [{'thread_id': '3889777',
                                'name': 'Look at this pigs huge ...',
                                'icon': {'icon_id': '913',
                                         'url': '...',
                                         'name': 'Balls'},
                                'visited': True,
                                'starred': False,
                                'stickied': True,
                                'max_pages': 51,
                                'author': 'Jose',
                                'replies': 2027,
                                'views': 124022,
                                'rating': {'url': '...',
                                           'info': '1140 votes - 4.33...'},
                                'killed_by': {'name': 'Empty Sandwich',
                                              'user_id': '000000',
                                              'datetime': Datetime()}
                               }]
                  }
            ]
            """
            soup = BeautifulSoup(webpage, features='lxml')
            return self.__forum_page_info(soup)
